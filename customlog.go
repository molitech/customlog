package customlog

import (
	"log"
	"os"
	"io"
)

type LogLevel int

const (
	INFO LogLevel = iota
	WARNING
	ERROR
)

type CustomLogger struct {
	Logger *log.Logger
	LogLevel LogLevel
}

func NewCustomLogger(out *os.File,prefix string,flag int,logLevel LogLevel) *CustomLogger{
	multiWriter := io.MultiWriter(os.Stdout,out)
	return &CustomLogger{
		Logger: log.New(multiWriter,prefix,flag),
		LogLevel: logLevel,
	}
}

func (c *CustomLogger) log(level LogLevel,message string){
	if level >= c.LogLevel{
		levelPrefix := ""
		switch level{
		case INFO:
			levelPrefix = "[INFO]"
		case WARNING:
			levelPrefix = "[WARNING]"
		case ERROR:
			levelPrefix = "[ERROR]"
		}
		c.Logger.Println(levelPrefix + message)
	}
}

func (c *CustomLogger) Info(message string){
	c.log(INFO,message)
}

func (c *CustomLogger) Warning(message string){
	c.log(WARNING,message)
}

func (c *CustomLogger) Error(message string){
	c.log(ERROR,message)
}

func (c *CustomLogger) Println(message string){
	c.log(INFO,message)
}